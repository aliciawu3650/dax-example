import numpy as np

from dax.experiment import *
from dax.interfaces.operation import OperationInterface
from dax.sim.time import *
from artiq.language.core import now_mu

from dax_example.modules.detection import DetectionModule
from dax_example.modules.trap import TrapModule
from dax_example.services.state import StateService

from qiskit import Aer, transpile, QuantumCircuit


class OperationService(DaxService, OperationInterface):


    SERVICE_NAME = 'operation'

    def build(self):
        # Add kernel invariants for properties
        self.update_kernel_invariants('pi', 'num_qubits', '_channel_map')

        # Required modules and services
        self._detect = self.registry.find_module(DetectionModule)
        self._trap = self.registry.find_module(TrapModule)
        self._state = self.registry.get_service(StateService)
        self.update_kernel_invariants('_detect', '_trap', '_state')

    # do we need 
    # def __init__(self, *args, **kwargs):
    # do we need to call super().__init__(self, *args, **kwargs)

    def init(self) -> None:
        self.timing = DaxTimeManager(0.1)
        self.op_list = []
        self.implicit_queue = []
        self.mz_times = []
        self.mz_count = 0
        self.new_state = None
        self.start_time = now_mu()


    def post_init(self) -> None:
        pass

    """Service functionality"""

    # optional arg is delay time - if positive, compare times & insert, keep track of current time, either append or insert

    @rpc(flags={'async'})
    def _log_gate(self, name, *qubits, **kwargs):
        if any(q < 0 or q >= 5 for q in qubits):
            raise IndexError('Qubit index out of range')
        args = [f'{k}={v}' for k, v in kwargs.items()]
        args.extend((str(q) for q in qubits))
        self.logger.info(f'{name}({",".join(args)})')
        # how to check if kwargs = none, if __ in kwargs.keys() -> add to tuple
        angle = kwargs.get('theta', None)
        # check the get time function, using get_time_mu(),maybe now_mu()
        self.op_list.append((now_mu(), name, qubits, angle))

    @kernel
    def i(self, qubit: TInt32):
        self._log_gate('I', qubit)

    @kernel
    def x(self, qubit: TInt32):
        self._log_gate('X', qubit)

    @kernel
    def y(self, qubit: TInt32):
        self._log_gate('Y', qubit)

    @kernel
    def z(self, qubit: TInt32):
        self._log_gate('Z', qubit)

    @kernel
    def h(self, qubit: TInt32):
        self._log_gate('H', qubit)

    @kernel
    def sqrt_x(self, qubit: TInt32):
        self._log_gate('sqrt_X', qubit)

    @kernel
    def sqrt_x_dag(self, qubit: TInt32):
        self._log_gate('sqrt_X_dag', qubit)

    @kernel
    def sqrt_y(self, qubit: TInt32):
        self._log_gate('sqrt_Y', qubit)

    @kernel
    def sqrt_y_dag(self, qubit: TInt32):
        self._log_gate('sqrt_Y_dag', qubit)

    @kernel
    def sqrt_z(self, qubit: TInt32):
        self._log_gate('sqrt_Z', qubit)

    @kernel
    def sqrt_z_dag(self, qubit: TInt32):
        self._log_gate('sqrt_Z_dag', qubit)

    @kernel
    def rx(self, theta: TFloat, qubit: TInt32):
        self._log_gate('rX', qubit, theta=theta)

    @kernel
    def ry(self, theta: TFloat, qubit: TInt32):
        self._log_gate('rY', qubit, theta=theta)

    @kernel
    def rz(self, theta: TFloat, qubit: TInt32):
        self._log_gate('rZ', qubit, theta=theta)

    @kernel
    def prep_0_all(self):
        self._trap.cool_pulse()

    @kernel
    def m_z_all(self):
        self._detect.detect_active()
        # for q in self._channel_map:
        for q in range(5):
            mz_time = now_mu()
            self.op_list.append((mz_time, "MZ", q, None))
            self.mz_times.append((mz_time, q))
        self.mz_count += 5

    @kernel
    def m_z(self, qubit: TInt32):
        mz_time = now_mu()
        self.op_list.append((mz_time, "MZ", qubit, None))
        self.mz_times.append((mz_time, qubit))
        self.mz_count += 1

    @kernel
    #def get_measurement(self, qubit: TInt32) -> TInt32:
    def get_measurement(self, qubit: TInt32):
        # sorting keeps insertion order on ties
        self.op_list.sort(key=lambda x: x[0])
        self.mz_times.sort(key=lambda x: x[0])
        if self.mz_count == 0:
            raise ValueError("No measurement gates logged")
            
        measurement_time = None
        print("Searching for qubit to measure in mz_times, qubit:", qubit)
        cnt = 0
        for (time, q_s) in self.mz_times:
            print("cnt: ", cnt)
            cnt+=1
            print("Time:", time)
            print("Qubit:", q_s)
            if qubit == q_s:
                measurement_time = time
                print("Found, measurement time is", measurement_time)
                break 
            
        if measurement_time == None: 
            raise ValueError("No measurement gate logged for this qubit")

        if self.implicit_queue: # if not empty
            print("Searching in implicit mz gates queue")
            for (time, mz_q, m_result, state) in self.implicit_queue.copy():
                if qubit == mz_q and time < self.curr_time:
                    print("Measurement Time", time)
                    print("Implicit Gate on qubit", mz_q)
                    print("Measurement result: ", m_result)
                    print("Resulting state: ", state) 
                    # tup = (time, mz_q, m_result, state)
                    self.implicit_queue.pop(0)
                    return

        found = False
        curr_time = None
        start_count = self.mz_count
        print("num of mz gates: ", start_count)
        qubit_measured = None # (rng program, somehow never goes to set qubit_measured)
        for i in range(start_count): 
            qc = QuantumCircuit(5, 5)
            #qc = QuantumCircuit(q.num_qubits(), q.num_qubits())
            if self.new_state is not None:
                qc.set_statevector(self.new_state)
                qc.save_state()
            curr_time = self.start_time
            print("current time now set to: ", curr_time)
            result = None

            operations = {
                "I": qc.i,
                "H": qc.h,
                "X": qc.x,
                "Y": qc.y,
                "Z": qc.z,
                "rX": qc.rx,
                "rY": qc.ry,
                "rZ": qc.rz,
                "sqrt_Z": qc.s,
                "sqrt_Z_dag": qc.sdg,
                "sqrt_Y": qc.ry,
                "sqrt_Y_dag": qc.ry,
                "sqrt_X": qc.rx,
                "sqrt_X_dag": qc.rx,
                # qc.ry(np.pi/4, q)
                # sqrt_Y_dag: qc.ry(-np.pi/4, q)
            }

            for (time, op, qlist, theta) in self.op_list.copy():
                print("operation is ", op)
                print("time:", time)
                # print("current time:", curr_time)
                if time < curr_time:
                    raise ValueError("Invalid operation timing")
                if op != "MZ": 
                    operation = operations.get(op, None)
                    if not operation:
                        raise NameError("Invalid operation name")
                    if op in ("sqrt_Y", "sqrt_X"):
                        for q in qlist:
                            operation(np.pi / 4, q)
                    elif op in ("sqrt_Y_dag", "sqrt_X_dag"):
                        for q in qlist:
                            operation(-np.pi / 4, q)
                    elif op in ("rX", "rY", "rZ"):
                        for q in qlist:
                            operation(theta, q)
                    else: 
                        for q in qlist:
                            operation(q)
                    self.op_list.pop(0)
                else: # measurement gate
                    if measurement_time == time and qlist == qubit:
                        found = True
                        qubit_measured = qubit
                        qc.measure(qubit, qubit)
                        self.mz_count -= 1
                        self.op_list.pop(0)
                        self.mz_times.pop(0)
                        self.start_time = measurement_time
                        break
                    # qc.save_statevector(label=f'measurement_{i}_on_qubit_{qlist}')
                    qc.measure(qlist, qlist)
                    qubit_measured = qlist
                    self.mz_count -= 1
                    self.mz_times.pop(0)
                    self.op_list.pop(0)
                    self.start_time = measurement_time
                    break
            simulator = Aer.get_backend("statevector_simulator")
            qc = transpile(qc, simulator)
            result = simulator.run(qc, shots=1, memory=True).result()
            # print('State before measurement:', result.data(0))

            # measurement output
            m_result = result.get_memory(qc)[0][5-(1+qubit_measured)] #self.q.num_qubits
            print("Measurement Result: ", m_result)
            # statevector output
            self.new_state = result.get_statevector()
            print("Resulting state: ", self.new_state)
            if found:
                break
            self.implicit_queue.append((time, qlist, m_result, self.new_state))
        return m_result
        #return 0
        #return self._detect.measure(self._channel_map[qubit])

    @kernel
    def store_measurements(self, qubits: TList(TInt32)):
        self._state.measure_channels([self._channel_map[q] for q in qubits])

    @property
    def num_qubits(self) -> np.int32:
        return np.int32(self._trap.num_ions())

    @property
    def _channel_map(self):
        """A map to convert qubit index to channel."""
        return self._detect.active_channels()

    @host_only
    def set_realtime(self, realtime: bool) -> None:
        pass