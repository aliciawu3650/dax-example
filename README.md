# DAX example project

This is an example project to demonstrate the features of [DAX](https://gitlab.com/duke-artiq/dax). The project is
configured to run in simulation by default (using DAX.sim) such that users can run the example code without the need for
any hardware. The design principles showcased in this example project are directly inspired by how we use DAX in our
lab.

In case you have any questions or comments about this example project, feel free to reach out to us by opening an issue.

## Project setup

Below you will find instructions to set up this example project in Nix or Conda. We assume you are already familiar with
the setup of [ARTIQ](https://github.com/m-labs/ARTIQ).

### Nix

This project contains a Nix shell script that can be used to set up the correct environment. Make sure the `artiq-full`
and `nixpkgs` channels are configured as described in the [ARTIQ manual](https://www.m-labs.hk/artiq/manual/). The DAX
packages will be fetched from git and build locally.

```shell
$ git clone https://gitlab.com/duke-artiq/dax-example.git
$ cd dax-example/
$ nix-shell
$ artiq_session
```

### Conda

We provided a Conda environment file to set up the correct environment. The DAX packages will be fetched from git and
installed using pip.

```shell
$ git clone https://gitlab.com/duke-artiq/dax-example.git
$ cd dax-example/
$ conda env create -f environment.yml
$ conda activate dax_example
$ artiq_session
```

## Usage

After starting the ARTIQ session, the dashboard will appear and experiments can be submitted through the dashboard. We
listed a few notes and tips for using the DAX example project:

- Some experiments open applets for live plotting, but by default, the global CCB policy is set to create applets only.
  Set the global CCB policy to "Create and enable/disable applets" to make sure applets are enabled and disabled
  automatically. The CCB policy can be changed by right-clicking the applets panel.
- Set the logging level of experiments to `INFO` to see more messages.

## Simulation

By default, this project runs in simulation using our DAX.sim simulation infrastructure. Most configuration for the
simulator can be found in [setup.cfg](setup.cfg). Users can override the configuration of DAX.sim locally by creating
a `.dax` file with their system-specific configuration. Note that the simulation is not bound to the wall-clock time and
simulated execution can be much faster than execution on actual hardware.

When running experiments in simulation, DAX.sim generates VCD files which are stored in the ARTIQ `results` directory.
VCD files can be opened with [GTKWave](http://gtkwave.sourceforge.net/)
and we provided a generated [GTKWave save file](waves.gtkw) to load a convenient viewer state.

## Testing

With the help of DAX.sim it is possible to perform automated testing on the code of this project. Execute the following
command in the root directory of this project to run the unit tests.

```shell
$ pytest
```

The environment of this project also includes [flake8-artiq](https://gitlab.com/duke-artiq/flake8-artiq). To run flake8,
execute the following command.

```shell
$ flake8
```

Tests can also be executed as part of a CI system. Our CI configuration can be found in [gitlab-ci.yml](.gitlab-ci.yml).

## Main contributors

- Leon Riesebos (Duke University)
