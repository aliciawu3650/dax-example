{ pkgs ? import <nixpkgs> {} }:

let
  artiq-full = import <artiq-full> { inherit pkgs; };
  dax-full = import <dax-full> {inherit pkgs; };
  drew-nur = import <drew-nur> {inherit pkgs; };
  daxVersion = "6.3";
  daxSrc = builtins.fetchGit {
    url = "https://gitlab.com/duke-artiq/dax.git";
    ref = "v${daxVersion}";
  };
  dax = pkgs.callPackage (import daxSrc) { artiqpkgs=artiq-full; inherit pkgs daxVersion; };
  dax-applets = pkgs.callPackage (import (builtins.fetchGit "https://gitlab.com/duke-artiq/dax-applets.git")) { artiqpkgs=artiq-full; inherit pkgs; };
  flake8-artiq = pkgs.callPackage (import (builtins.fetchGit "https://gitlab.com/duke-artiq/flake8-artiq.git")) { inherit pkgs; };
in
  pkgs.mkShell {
    buildInputs = [
      # Python packages
      (pkgs.python3.withPackages(ps: [
        # ARTIQ
        artiq-full.artiq
        artiq-full.artiq-comtools
        # DAX
        dax
        dax-applets
        # Testing packages
        ps.pytest
        ps.flake8
        flake8-artiq
        # drewnur packages
        drew-nur.qiskit
      ]))
    ];
  }
