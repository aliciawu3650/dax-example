# Tests

This directory contains unit tests based on the test case infrastructure of DAX.sim. The test case classes of DAX.sim
are an extension of the standard Python `unittest` classes and therefore integrate seamlessly with existing software
tools for testing. DAX.sim allows users to construct ARTIQ environments and test the state of devices during simulation.
