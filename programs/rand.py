from dax.program import *
from artiq.language.core import delay
import numpy as np

class Rand(DaxProgram, Experiment):

    @kernel
    def run(self):
        self.core.reset()
        self.q.prep_0_all()

        num_gates = 200
        num_qubits = 4

        operations = {
            "MZ": self.q.m_z,
            "GM": self.q.get_measurement,
            "I": self.q.i,
            "H": self.q.h,
            "X": self.q.x,
            "Y": self.q.y,
            "Z": self.q.z,
            "rX": self.q.rx,
            "rY": self.q.ry,
            "rZ": self.q.rz,
            "sqrt_Z": self.q.sqrt_z,
            "sqrt_Z_dag": self.q.sqrt_z_dag,
            "sqrt_Y": self.q.sqrt_y,
            "sqrt_Y_dag": self.q.sqrt_y_dag,
            "sqrt_X": self.q.sqrt_x,
            "sqrt_X_dag": self.q.sqrt_x_dag,
        }

        for i in range(num_gates):
            op_num = np.random.randint(0, 16)
            print("op num:", op_num)
            op = list(operations)[op_num]
            rand_op = operations.get(op, None)
            rand_qubit = np.random.randint(0, num_qubits)
            print("rand_qubit:", rand_qubit)
            if op_num in (7, 8, 9):
                rand_int = np.random.randint(1, 11)
                rand_op(self.q.pi/rand_int, rand_qubit)
            else:
                rand_op(rand_qubit)
            rand_delay = np.random.randint(0, 11)
            rand_delay = rand_delay - 5
            delay(rand_delay * s)
            # positive delay greater than 10 ns have to add another 1 ns delay
