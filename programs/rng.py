"""
A program for a bit-sequential random number generator.
"""

from dax.program import *


class Rng(DaxProgram, Experiment):

    def run(self):
        result = self.rng(5)
        self.logger.info(f'Result: {result}')

    @kernel
    def rng(self, num_bits):
        self.core.reset()
        result = 0

        for _ in range(num_bits):
            self.core.break_realtime()
            self.q.prep_0_all()
            self.q.h(0)
            self.q.m_z_all()

            measurements = [self.q.get_measurement(i) for i in range(num_bits)]
            result <<= 1
            result += measurements[0]

        return result
