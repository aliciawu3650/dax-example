"""
A program demonstrating it is possible to include a negative delay
"""

from dax.program import *
from artiq.language.core import delay
import numpy as np

class NegativeDelay(DaxProgram, Experiment):

    @kernel
    def run(self):
        self.core.reset()
        self.q.prep_0_all()

        self.q.h(0)
        delay(5 * s)
        self.q.m_z(0)
        delay(-3 * s)
        self.q.rx(np.pi/2, 0)
        delay(6 * s)
        self.q.y(0) # has an earlier time than m_z
        delay(1 * s)
        self.q.get_measurement(0)


        # self.core.reset()
        # self.q.prep_0_all()

        # self.q.h(0)
        # delay(5 * s)
        # self.q.m_z(0)
        # delay(-3 * s)
        # self.q.x(0)
        # delay(1 * s)
        # self.q.rx(np.pi/2, 0)
        # delay(2 * s)
        # self.q.y(0) # has an earlier time than m_z
        # delay(1 * s)
        # self.q.get_measurement(0)
        # delay(5 * s)
        # self.q.h(0)
        # delay(2 * s)
        # self.q.sqrt_y(1)
        # delay(-1 * s)
        # self.q.sqrt_z_dag(1)
        # delay(1 * s)
        # self.q.m_z(1)
        # delay(1 * s)
        # self.q.get_measurement(1)

        ## also example with bad tim example to show it accounts for it

