"""
A program demonstrating various available gates.
"""
from dax.program import *
from pathlib import Path
import numpy as np


class Gates(DaxProgram, Experiment):

    @kernel
    def test_gates(self):
        self.core.reset()
        self.q.prep_0_all()

        self.q.i(0)
        self.q.x(0)
        self.q.y(0)
        self.q.z(0)
        self.q.h(0)

        self.q.sqrt_x(0)
        self.q.sqrt_x_dag(0)
        self.q.sqrt_y(0)
        self.q.sqrt_y_dag(0)
        self.q.sqrt_z(0)
        self.q.sqrt_z_dag(0)

        self.q.rx(self.q.pi / 4, 0)
        self.q.ry(self.q.pi / 4, 0)
        self.q.rz(self.q.pi / 4, 0)

        self.q.m_z(0)
        (result, state) = self.q.get_measurement(0)
        backend_state = np.array_str(state, precision = 10, suppress_small = False)
        expected_state = Path('/home/aliciawu/LAB/dax-example/programs/expected_output/gates_expected.txt').read_text()
        print(backend_state)#
        print("next\n")
        print(expected_state)
        assert backend_state == expected_state, "Should be same"

        #self.core.wait_until_mu(now_mu())
        #assert ([1, 2, 3]) == 6, "Should be 6"

    if __name__ == "__main__":
        run()
        print("Everything passed")
