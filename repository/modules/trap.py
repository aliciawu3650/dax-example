"""
The trap experiment demonstrates how the trap module can be used.
"""

from dax_example.system import *


class TrapExperiment(DaxExampleSystem, Experiment):
    """Trap module"""

    def build(self):
        # Call super
        super(TrapExperiment, self).build()

        # Get arguments
        self._num_pulses = self.get_argument('Num pulses', NumberValue(8, min=1, step=1, ndecimals=0))
        self._pulse_time = self.get_argument('Pulse time',
                                             NumberValue(100 * us, unit='us', min=0 * us, step=10 * us))
        self._delay_time = self.get_argument('Delay time',
                                             NumberValue(100 * us, unit='us', min=0 * us, step=10 * us))
        self._freq = self.get_argument('Cooling DDS frequency',
                                       NumberValue(100 * MHz, unit='MHz', min=0 * MHz, max=400 * MHz, step=10 * MHz))
        self.update_kernel_invariants('_num_pulses', '_pulse_time', '_delay_time', '_freq')

    def run(self):
        # Initialize system
        self.dax_init()

        # Run kernel
        self._run()

    @kernel
    def _run(self):
        # Reset core
        self.core.reset()

        # Switch of cooling
        self.trap.cool_off()
        # Configure cooling DDS
        self.trap.cool_config(self._freq)

        for _ in range(self._num_pulses):
            # Add delay
            delay(self._delay_time)
            # Pulse
            self.trap.cool_pulse(self._pulse_time)

        # Reset cooling DDS configuration
        self.trap.cool_reset()

        # Wait until all events have been submitted
        self.core.wait_until_mu(now_mu())
