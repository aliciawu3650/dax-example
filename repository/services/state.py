"""
DAX contains data-processing modules that can be used as part of of your DAX system.
These data-processing modules are able to collect, process, and plot data for specific use-cases.
The state service experiment demonstrates how the histogram data-processor (part of the state service) is used.
"""

from dax.modules.hist_context import HistogramAnalyzer

from dax_example.system import *


class StateExperiment(DaxExampleSystem, Experiment):
    """State service"""

    def build(self):
        # Call super
        super(StateExperiment, self).build()

        # Get arguments
        self._detection_time = self.get_argument('Detection time',
                                                 NumberValue(100 * us, unit='us', min=0 * us, step=10 * us))
        self._num_samples = self.get_argument('Num samples', NumberValue(100, min=1, step=1, ndecimals=0))
        self._plot_histogram = self.get_argument('Plot histogram', BooleanValue(True))
        self.update_kernel_invariants('_detection_time', '_num_samples')

    def run(self):
        # Initialize system
        self.dax_init()

        if self._plot_histogram:
            # Open histogram applet
            self.state.context.plot_histogram()

        # Run kernel
        self._run()

    @kernel
    def _run(self):
        # Reset core
        self.core.reset()

        with self.state.context:
            for i in range(self._num_samples):
                self.trap.cool_pulse()
                self.detection.detect_active(self._detection_time)
                self.state.count_active()
                delay(10 * us)

        # Leave cooling on
        self.trap.cool_on()
        self.core.wait_until_mu(now_mu())

    def analyze(self):
        # Save histograms as PDF files in the results directory
        a = HistogramAnalyzer(self)
        a.plot_all_histograms()
