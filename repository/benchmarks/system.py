"""
DAX has a notion of clients which are abstract experiments that can be instantiated against a DAX system.
This file demonstrates how standard DAX clients can be connected to the DAX example system to instantiate an experiment.
The experiments in this file are benchmarks to measure performance of system building and system initialization.
"""

from dax.clients.system_benchmark import *

from dax_example.system import DaxExampleSystem


# noinspection PyTypeChecker
class DaxInit(SystemBenchmarkDaxInit(DaxExampleSystem)):
    pass


# noinspection PyTypeChecker
class DaxInitProfile(SystemBenchmarkDaxInitProfile(DaxExampleSystem)):
    pass


class BuildProfile(SystemBenchmarkBuildProfile(DaxExampleSystem)):
    pass
